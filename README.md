All Mangas Reader mirrors implementation repository
=======

View the magic behind how we control our mirrors and push your contributions.

Mirrors implementations are in mirrors, icons for websites are in icons.
To update implementations or create new one, follow these steps : 
 - fork this repository (or just clone it locally)
 - run a local server at the root of your local repo, to do so, you can install http-server through npm and then run the server (the server needs to run on https because of the extension CSP which are not allowed on http protocol. The working certificate is in the repository, you can regenerate it through makecert.bat on windows, the certificate (localhost.crt) needs to be added to your trusted root certification authorities on your system : for example through chrome > settings > manage certificates > tab "trusted root certification authorities" > Import. You need to restart browsers and server after importing certificate) : 
```
    npm install http-server -g
    http-server -S -C localhost.crt -K localhost.key
```

 - create the websites.json file using node : 
```
    node .\update-ws.js
```

 - Configure your local repository in AMR (in the extension, open options > Supported websites > at the bottom click "New Repository" and enter https://localhost:8080)
 - Go to the lab (in the extension, open options > Supported websites > at the bottom, click on AMR Laboratory)
 - Click on Refresh Mirrors to update AMR mirrors list, select your mirror and run tests

You can quickly debug issues by changing code, reloading mirrors, running tests. Sometimes, if you use console.log to debug, it appears in the background.html log

Don't forget to run "node ./update-ws.js" if you update implementations attributes

Please keep the files updated and pull requests for your updates / new implementations